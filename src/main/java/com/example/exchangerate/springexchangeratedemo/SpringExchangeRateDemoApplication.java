package com.example.exchangerate.springexchangeratedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringExchangeRateDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringExchangeRateDemoApplication.class, args);
    }

}
