package com.example.exchangerate.springexchangeratedemo.controller.view;

import java.math.BigDecimal;

public class ExchangeRequest {
    private final String baseCurrency;
    private final String targetCurrency;
    private final BigDecimal value;

    public ExchangeRequest(String baseCurrency, String targetCurrency, BigDecimal value) {
        this.baseCurrency = baseCurrency;
        this.targetCurrency = targetCurrency;
        this.value = value;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public BigDecimal getValue() {
        return value;
    }
}
