package com.example.exchangerate.springexchangeratedemo.controller.view;

import java.math.BigDecimal;

public class ExchangeResponse {
    private final String baseCurrency;
    private final String currency;
    private final BigDecimal exchangedValue;

    public ExchangeResponse(String baseCurrency, String currency, BigDecimal exchangedValue) {
        this.baseCurrency = baseCurrency;
        this.currency = currency;
        this.exchangedValue = exchangedValue;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getExchangedValue() {
        return exchangedValue;
    }
}
