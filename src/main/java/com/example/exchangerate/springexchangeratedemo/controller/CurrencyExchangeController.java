package com.example.exchangerate.springexchangeratedemo.controller;

import java.math.BigDecimal;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.exchangerate.springexchangeratedemo.controller.view.ExchangeRequest;
import com.example.exchangerate.springexchangeratedemo.controller.view.ExchangeResponse;
import com.example.exchangerate.springexchangeratedemo.exchangerates.ExchangeRateProvider;

@RestController
public class CurrencyExchangeController {

    private final ExchangeRateProvider exchangeRateProvider;

    public CurrencyExchangeController(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    @PostMapping("/exchange")
    public ResponseEntity<ExchangeResponse> exchange(@RequestBody ExchangeRequest request) {
        BigDecimal exchangeRate = exchangeRateProvider.provideExchangeRate(request.getBaseCurrency(), request.getTargetCurrency());
        return ResponseEntity.ok(new ExchangeResponse(request.getBaseCurrency(), request.getTargetCurrency(), calculateExchangedValue(request.getValue(), exchangeRate)));
    }

    private BigDecimal calculateExchangedValue(BigDecimal value, BigDecimal exchangeRate) {
        return value.multiply(exchangeRate);
    }

}
