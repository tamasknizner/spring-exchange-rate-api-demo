package com.example.exchangerate.springexchangeratedemo.exchangerates;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.example.exchangerate.springexchangeratedemo.exchangerates.client.ExchangeRateApiClient;

@Service
public class RestBasedExchangeRateProvider implements ExchangeRateProvider {

    private final ExchangeRateApiClient exchangeRateApiClient;

    public RestBasedExchangeRateProvider(ExchangeRateApiClient exchangeRateApiClient) {
        this.exchangeRateApiClient = exchangeRateApiClient;
    }

    @Override
    public BigDecimal provideExchangeRate(String baseCurrency, String targetCurrency) {
        return exchangeRateApiClient.getExchangeRates(baseCurrency).getConversionRates().get(targetCurrency);
    }
}
