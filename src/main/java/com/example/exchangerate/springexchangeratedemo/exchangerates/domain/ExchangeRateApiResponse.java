package com.example.exchangerate.springexchangeratedemo.exchangerates.domain;

import java.math.BigDecimal;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ExchangeRateApiResponse.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeRateApiResponse {
    private final String result;
    private final String documentation;
    private final String termsOfUse;
    private final Long timeLastUpdateUnix;
    private final String timeLastUpdateUtc;
    private final Long timeNextUpdateUnix;
    private final String timeNextUpdateUtc;
    private final String baseCode;
    private final Map<String, BigDecimal> conversionRates;
    private final String errorType;

    private ExchangeRateApiResponse(Builder builder) {
        this.result = builder.result;
        this.documentation = builder.documentation;
        this.termsOfUse = builder.termsOfUse;
        this.timeLastUpdateUnix = builder.timeLastUpdateUnix;
        this.timeLastUpdateUtc = builder.timeLastUpdateUtc;
        this.timeNextUpdateUnix = builder.timeNextUpdateUnix;
        this.timeNextUpdateUtc = builder.timeNextUpdateUtc;
        this.baseCode = builder.baseCode;
        this.conversionRates = builder.conversionRates;
        this.errorType = builder.errorType;
    }

    public String getResult() {
        return result;
    }

    public String getDocumentation() {
        return documentation;
    }

    public String getTermsOfUse() {
        return termsOfUse;
    }

    public Long getTimeLastUpdateUnix() {
        return timeLastUpdateUnix;
    }

    public String getTimeLastUpdateUtc() {
        return timeLastUpdateUtc;
    }

    public Long getTimeNextUpdateUnix() {
        return timeNextUpdateUnix;
    }

    public String getTimeNextUpdateUtc() {
        return timeNextUpdateUtc;
    }

    public String getBaseCode() {
        return baseCode;
    }

    public Map<String, BigDecimal> getConversionRates() {
        return conversionRates;
    }

    public String getErrorType() {
        return errorType;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    public static final class Builder {
        private String result;
        private String documentation;
        private String termsOfUse;
        private Long timeLastUpdateUnix;
        private String timeLastUpdateUtc;
        private Long timeNextUpdateUnix;
        private String timeNextUpdateUtc;
        private String baseCode;
        private Map<String, BigDecimal> conversionRates;
        private String errorType;

        private Builder() {
        }

        @JsonProperty("result")
        public Builder withResult(String result) {
            this.result = result;
            return this;
        }

        @JsonProperty("documentation")
        public Builder withDocumentation(String documentation) {
            this.documentation = documentation;
            return this;
        }

        @JsonProperty("terms_of_use")
        public Builder withTermsOfUse(String termsOfUse) {
            this.termsOfUse = termsOfUse;
            return this;
        }

        // Needed because the error response has terms of use with - instead of _
        @JsonProperty("terms-of-use")
        public Builder withTermsOfUseError(String termsOfUse) {
            this.termsOfUse = termsOfUse;
            return this;
        }

        @JsonProperty("time_last_update_unix")
        public Builder withTimeLastUpdateUnix(Long timeLastUpdateUnix) {
            this.timeLastUpdateUnix = timeLastUpdateUnix;
            return this;
        }

        @JsonProperty("time_last_update_utc")
        public Builder withTimeLastUpdateUtc(String timeLastUpdateUtc) {
            this.timeLastUpdateUtc = timeLastUpdateUtc;
            return this;
        }

        @JsonProperty("time_next_update_unix")
        public Builder withTimeNextUpdateUnix(Long timeNextUpdateUnix) {
            this.timeNextUpdateUnix = timeNextUpdateUnix;
            return this;
        }

        @JsonProperty("time_next_update_utc")
        public Builder withTimeNextUpdateUtc(String timeNextUpdateUtc) {
            this.timeNextUpdateUtc = timeNextUpdateUtc;
            return this;
        }

        @JsonProperty("base_code")
        public Builder withBaseCode(String baseCode) {
            this.baseCode = baseCode;
            return this;
        }

        @JsonProperty("conversion_rates")
        public Builder withConversionRates(Map<String, BigDecimal> conversionRates) {
            this.conversionRates = conversionRates;
            return this;
        }

        @JsonProperty("error-type")
        public Builder withErrorType(String errorType) {
            this.errorType = errorType;
            return this;
        }

        public ExchangeRateApiResponse build() {
            return new ExchangeRateApiResponse(this);
        }
    }
}
