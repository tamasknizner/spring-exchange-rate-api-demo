package com.example.exchangerate.springexchangeratedemo.exchangerates.configuration;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.example.exchangerate.springexchangeratedemo.exchangerates.interceptor.LoggerInterceptor;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    public RestTemplate exchangeRateApiClientRestTemplate(RestTemplateBuilder builder, ExchangeRateClientRootUriBuilder uriBuilder) {
        return builder
                .rootUri(uriBuilder.build())
                .interceptors(new LoggerInterceptor())
                .requestFactory(() -> new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()))
                .build();
    }
}
