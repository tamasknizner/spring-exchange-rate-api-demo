package com.example.exchangerate.springexchangeratedemo.exchangerates.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LoggerInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        LOGGER.info("{} - headers: {}, request body: {}", request.getURI().getPath(), request.getHeaders(), new String(body, StandardCharsets.UTF_8));
        ClientHttpResponse response = execution.execute(request, body);
        String responseBody = getResponseBody(response);
        LOGGER.info("response: {}", responseBody);
        return response;
    }

    private String getResponseBody(ClientHttpResponse response) throws IOException {
        InputStreamReader reader = new InputStreamReader(response.getBody(), StandardCharsets.UTF_8);
        return new BufferedReader(reader).lines()
                .map(String::strip)
                .collect(Collectors.joining());
    }
}

