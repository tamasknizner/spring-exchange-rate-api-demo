package com.example.exchangerate.springexchangeratedemo.exchangerates.client;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import com.example.exchangerate.springexchangeratedemo.exchangerates.domain.ExchangeRateApiResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ExchangeRateApiClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateApiClient.class);

    private final RestTemplate exchangeRateApiClientRestTemplate;

    public ExchangeRateApiClient(RestTemplate exchangeRateApiClientRestTemplate) {
        this.exchangeRateApiClientRestTemplate = exchangeRateApiClientRestTemplate;
    }

    public ExchangeRateApiResponse getExchangeRates(String baseCurrency) {
        ExchangeRateApiResponse response = null;
        try {
            response = exchangeRateApiClientRestTemplate.getForObject("/" + baseCurrency, ExchangeRateApiResponse.class);
        } catch (HttpStatusCodeException e) {
            handleError(e);
        }
        return response;
    }

    // far from perfect
    private void handleError(HttpStatusCodeException statusCodeException) {
        try {
            ExchangeRateApiResponse errorResponse = new ObjectMapper().readValue(statusCodeException.getResponseBodyAsByteArray(), ExchangeRateApiResponse.class);
            throw new ResponseStatusException(statusCodeException.getStatusCode(), errorResponse.getErrorType());
        } catch (IOException ioException) {
            LOGGER.warn("Exception while parsing error response: {}", ioException.getMessage());
        }
    }

}
