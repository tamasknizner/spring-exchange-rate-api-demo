package com.example.exchangerate.springexchangeratedemo.exchangerates;

import java.math.BigDecimal;

public interface ExchangeRateProvider {

    BigDecimal provideExchangeRate(String baseCurrency, String targetCurrency);

}
