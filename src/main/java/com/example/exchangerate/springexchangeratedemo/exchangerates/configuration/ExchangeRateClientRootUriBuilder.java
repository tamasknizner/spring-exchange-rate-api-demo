package com.example.exchangerate.springexchangeratedemo.exchangerates.configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ExchangeRateClientRootUriBuilder {

    private static final String API_DISCRIMINATOR = "latest";
    private static final String DELIMITER = "/";

    private final String url;
    private final String version;
    private final String apiKey;

    public ExchangeRateClientRootUriBuilder(
            @Value("${exchangerateapi.url}") String url,
            @Value("${exchangerateapi.version}") String version,
            @Value("${exchangerateapi.apiKey}") String apiKey) {
        this.url = url;
        this.version = version;
        this.apiKey = apiKey;
    }

    public String build() {
        return String.join(DELIMITER, List.of(url, version, apiKey, API_DISCRIMINATOR));
    }

}
